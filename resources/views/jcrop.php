<html>
    <head>
        <title>Laravel and Jcrop</title>
        <meta charset="utf-8">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/jquery.Jcrop.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
        <script src="js/jquery.Jcrop.min.js"></script>
    </head>
    <body>
        <div class="container">

            <h2>Image Cropping with Laravel and Jcrop</h2>
            <div class="form-group">
                <img src="<?php echo $image ?>" id="cropimage">
            </div>

            <?= Form::open() ?>
            <?= Form::hidden('image', $image) ?>
            <?= Form::hidden('x', '', array('id' => 'x')) ?>
            <?= Form::hidden('y', '', array('id' => 'y')) ?>
            <?= Form::hidden('w', '', array('id' => 'w')) ?>
            <?= Form::hidden('h', '', array('id' => 'h')) ?>
            <?= Form::submit('Crop It Now', ['class'=> 'btn btn-info']) ?>
            <?= Form::close() ?>
        </div>

        <script type="text/javascript">
            $(function() {
                $('#cropimage').Jcrop({
                    onSelect: updateCoords,
                    setSelect:   [ 0, 0, 900, 300 ],
                    aspectRatio: '',
                    minSize: [ 900, 300 ],
                    maxSize: [ 900, 300 ]
                });
            });
            function updateCoords(c) {
                $('#x').val(c.x);
                $('#y').val(c.y);
                $('#w').val(900);
                $('#h').val(300);
            };
        </script>
    </body>
</html>